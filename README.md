# Tutorial_LBC_git

Tutorial de comandos b�sicos de git y Markdown

###Practica de como usar y colaborar en un repositorio compartido.

#####Clonar el repositorio de forma local.

```
git clone
```

*Actualmente solo tiene el archivo de descripci�n en formato Markdown.

#####Crear un archivo nuevo

Crear un archivo de texto plano con tu nombre ejemplo `josian.txt`

```
git add josian.txt
git commit -m "mi primer archivo git"

#Revisar estatus de git

git status

#mandar al repositorio maestro 

git push origin master
```

*De la misma manera se pueden crear otros repositorios internos 

---
#####Control de cambios

Ahora todos han subido un archivo diferente que debe reflejarse en el repositorio principal, lo cual podemos checar directamente en la terminar o en la p�gina de *bitbucket*. 

```
git log

```

#####Actualizar

Actualizamos nuestro repositorio local para tener toda una copia de los archivos 

```
git pull

```
#####Modificar archivos 

Abriremos nuestro archivo, agregaremos y editaremos unas l�neas.

*La edici�n de los archivos puede ser en el editor de texto o c�digo de tu preferencia 

Ahora miraremos las diferencias con nuestro commit anterior.

```
git diff master

# Subir las modificaciones al repositorio maestro

git add josian.tex; git commit -m "Update lines"
git status
git push -u origin master

```
*Recuerda actualizar tu repositorio con `git pull`

---
#####Resolver conflictos 

Repetir el paso anterior y hacer modificaciones en la linea inicial a su archivo y hacer `commit`.


Ahora modifica el archivo de uno de tus compa�eros exactamente en la linea inicial. 

*Podriamos resolver el conflicto borrando nuestras modificaciones con `git checkout` o seleccionar manualmente cual es la vercion correcta.


Tratar de subir las modificaciones a el repositorio maestro 


####Ejercicio final

1.Editar y corregir errores en `README.md`.
2.Agregar informaci�n que crean importante para futuros cursos.

#####Cursos recomendados 
Link:


[La gu�a sencilla git](http://rogerdudler.github.io/git-guide/index.es.html)

[Version Control with Git](https://swcarpentry.github.io/git-novice/)

[Markdownguide](https://www.markdownguide.org/basic-syntax/)


